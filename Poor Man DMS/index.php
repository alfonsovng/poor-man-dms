<?php
/* Poor Man DMS v1.0
 * Copyright 2014 Alfonso da Silva (alfonsodasilva@gmail.com)
 * Licensed under MIT (http://git.shotbingo.com/poor-man-dms/raw/master/Poor%20Man%20DMS/license.txt)
 */
include 'config.php';

$rootdir = "";
$errorMsg = "";
if(isset($_GET ['errorMsg'])) {
	$errorMsg = urldecode($_GET ['errorMsg']);
}

if (isset ( $_POST ['password'] )) {
	unset ( $_SESSION ['rootdir'] );
	
	$password = $_POST ['password'];
	$dh = opendir ( PMDMS_FILES_DIR );
	while ( ($file = readdir ( $dh )) !== false ) {
		if ($file != ".." && $file != "." && is_dir ( PMDMS_FILES_DIR . DIRECTORY_SEPARATOR . $file )) {
			$items = explode ( ".", $file );
			if (md5 ( $password . PMDMS_PASSWORD_SUFFIX ) == $items[1]) {
				$rootdir = PMDMS_FILES_DIR . DIRECTORY_SEPARATOR . $file;
				$_SESSION ['rootdir'] = $rootdir;
				break;
			}
		}
	}
	
	if ($rootdir == "") {
		$errorMsg = PMDMS_WRONG_PASSWORD;
	}
} else if (isset ( $_POST ['logout'] )) {
	unset ( $_SESSION ['rootdir'] );
	$rootdir = "";
} else if (isset ( $_SESSION ['rootdir'] )) {
	$rootdir = $_SESSION ['rootdir'];
	if (! is_dir ( $rootdir )) {
		unset ( $_SESSION ['rootdir'] );
		$rootdir = "";
		$errorMsg = PMDMS_ACCESS_ERROR;
	}
}

function scanDirWithFileRegexp($rootdir, $dir) {
	$result = array();

	$dirContent = scandir($rootdir . $dir);
	foreach ($dirContent as $key => $file) {
		if (!in_array($file, array(".",".."))) {
			if (is_dir($rootdir . $dir . DIRECTORY_SEPARATOR . $file)) {
				$result[$file] = scanDirWithFileRegexp($rootdir, $dir . DIRECTORY_SEPARATOR . $file);
			}
			else if (preg_match (PMDMS_DOWNLOADABLE_FILE_REGEXP, $file)) {
				$result[$file] = $dir . DIRECTORY_SEPARATOR . $file;
			}
		}
	}
	return $result;
}

function writeTable($buitsAbans, $buitsDespres, $checkBoxClass, &$checkBoxClassCounter, $dataArray) {
	foreach ($dataArray as $k=>$v) {
		$currentCheckBoxClass = $checkBoxClass;
		
		for($i = 0; $i < $buitsAbans; $i ++) {
			echo "<td class=\"pmdms_empty\"></td>\n";
		}
		
		$folder = is_array($v);
		if($folder) {
			$currentCheckBoxId = "pmdms_cb".$checkBoxClassCounter;
			echo "<td class=\"pmdms_check\"><input type=\"checkbox\" class=\"".$currentCheckBoxClass."\" id=\"" . $currentCheckBoxId . "\"></input></td>\n";
			echo "<td class=\"pmdms_folder\">" . $k . "</td>\n";
			echo "<script language='javascript'>\n";
			echo "$(document).ready(function(){";
			echo '$("#' .$currentCheckBoxId . '").click(function () {';
			echo '$(".' .$currentCheckBoxId . '").prop("checked", $(this).prop("checked"));';
			echo "})});\n";
			echo "$(document).ready(function(){";
			echo '$(".' .$currentCheckBoxId . '").click(function () {';
			echo 'if(!this.checked) $("#' .$currentCheckBoxId . '").prop("checked", $(this).prop("checked"));';
			echo "})});\n";
			echo "</script>\n";
			
			$checkBoxClassCounter++;
			$currentCheckBoxClass = $currentCheckBoxClass." ".$currentCheckBoxId;
		} else {
			echo "<td class=\"pmdms_check\"><input type=\"checkbox\" name=\"file[]\" value=\"".$v."\" class=\"" . $currentCheckBoxClass . "\"></input></td>\n";
			echo "<td class=\"pmdms_file\"><a href=\"download.php?file=".urlencode($v)."\">" . $k . "</a></td>\n";
		}
		
		for($i = 0; $i < $buitsDespres; $i ++) {
			echo "<td class=\"pmdms_empty\"></td>\n";
		}
		echo "<td class=\"pmdms_endrow\"></td>\n";
		echo "</tr>\n";
		
		if($folder) {
			writeTable($buitsAbans + 1, $buitsDespres -1, $currentCheckBoxClass, $checkBoxClassCounter, $v);
		}
	}
}
?>
<!--
/* Poor Man DMS v1.0
 * Copyright 2014 Alfonso da Silva (alfonsodasilva@gmail.com)
 * Licensed under MIT (https://bitbucket.org/alfonsovng/poor-man-dms/raw/master/Poor%20Man%20DMS/license.txt)
 */
 -->
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="author" content="alfonsodasilva@gmail.com">
 	<!-- 
      <link rel="shortcut icon" href="../../assets/ico/favicon.ico">
      -->
<title><?= PMDMS_H1_TEXT ?></title>
<!-- Bootstrap core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<style type="text/css">
body {
	padding-top: 50px;
	padding-bottom: 20px;
}

td {
	white-space: nowrap;
}

.pmdms_folder {
	width: auto;
	font-weight: bold;
}

.pmdms_file {
	width: auto;
}

.pmdms_check {
	text-align: right;
}

.pmdms_empty {
	width: 0px;
}

.pmdms_endrow {
	width: 100%;
}
</style>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
	<div class="navbar navbar-inverse navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="<?= PMDMS_MAIN_PAGE_URL ?>" target="<?= PMDMS_MAIN_PAGE_TARGET ?>"><?= PMDMS_MAIN_PAGE_NAME ?></a>
			</div>
			<div class="navbar-collapse collapse">
				<!--  PUT HERE YOUR NAVIGATION LINKS -->
				<!-- 
				<ul class="nav navbar-nav">
					<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Search engines <b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li><a
								href="http://www.google.com"
								target="_blank">Google</a></li>
							<li><a
								href="http://www.bing.com"
								target="_blank">Bing</a></li>
							<li><a
								href="http://www.yahoo.com"
								target="_blank">Yahoo</a></li>
						</ul>
					</li>
				</ul>
				-->
				<form class="navbar-form navbar-right" method="post" action=".">
<?php if($rootdir == "") { ?>
            <div class="form-group">
						<input type="password" name="password"
							placeholder="<?= PMDMS_PASSWORD_PLACE_HOLDER_TEXT ?>"
							class="form-control">
					</div>
					<button type="submit" class="btn btn-success"><?= PMDMS_LOGIN_BUTTON_TEXT ?></button>
<?php } else { ?>
			<input type="hidden" name="logout" value="true" />
					<button type="submit" class="btn btn-danger"><?= PMDMS_LOGOUT_BUTTON_TEXT ?></button>
<?php } ?>
          </form>
			</div>
		</div>
	</div>
	<div class="container">
		<?php if($errorMsg != "") { ?>
			<p id="errorMsg" class="alert alert-danger"><?= $errorMsg ?></p>
			<script language='javascript'>
			$(document).ready(function() {
			    $('#errorMsg').delay(3000).animate({opacity:0,height:0,padding:0,margin:0}, 'slow', function() {
			    	 $(this).remove();
			    });
			    return false;
			});
			</script>
		<?php }?>
		&nbsp;
	</div>
	<div class="container">
<?php if($rootdir == "") { ?>
    	<div class="jumbotron">
			<h1><?= PMDMS_H1_TEXT ?></h1>
			<p><?= PMDMS_LOGIN_PAGE_TEXT ?></p>
		</div>
<?php } else { ?>
		<h1 class="h2"><?= PMDMS_H1_TEXT ?></h1>
		<form method="post" action="download.php">
			<div class="table-responsive">
				<table class="table table-striped">
					<tbody>
	<?php
	preg_match("/\/([^\.]+)\./",$rootdir,$dir);	
	
	$data = array($dir[1] => scanDirWithFileRegexp($rootdir, ""));
	
	//profundidad máxima, 4...	
	echo "<!-- ".$dir[1]." -->";
	$checkBoxClassConter = 0;
	writeTable(0, 4, "pmdms_cb", $checkBoxClassConter, $data);
	?>
				</tbody>
				</table>
				<div class="form-group">
					<button type="submit" class="btn btn-info"><?= PMDMS_ZIP_BUTTON_TEXT ?></button>
				</div>
			</div>
		</form>
<?php } ?>
		<hr />
		<footer>
			<p class="text-muted">
				<a href="http://git.shotbingo.com/poor-man-dms/" target="_blank">Poor
					Man DMS</a> by <a href="mailto:alfonsodasilva@gmail.com">alfonsodasilva@gmail.com</a>
			</p>
		</footer>
	</div>
</body>
</html>
