<?php
/* Poor Man DMS v1.0
 * Copyright 2014 Alfonso da Silva (alfonsodasilva@gmail.com)
 * Licensed under MIT (http://git.shotbingo.com/poor-man-dms/raw/master/Poor%20Man%20DMS/license.txt)
 */
session_start();

//suffix to append to the password used to generate the md5
define("PMDMS_PASSWORD_SUFFIX", "123");

//directoris
define("PMDMS_FILES_DIR", "files");
define("PMDMS_TMP_DIR", "tmp");

//just 1 type of docs... 
define("PMDMS_DOWNLOADABLE_FILE_REGEXP", "/^\d\d\..+\.pdf$/i");
define("PMDMS_DOWNLOADABLE_FILE_MIME_TYPE", "application/pdf");
define("PMDMS_DOWNLOADABLE_FILE_PREFIX", "PMDMS");

//main page
define("PMDMS_MAIN_PAGE_NAME", "PMDMS Demo site");
define("PMDMS_MAIN_PAGE_URL", "http://www.shotbingo.com/");
define("PMDMS_MAIN_PAGE_TARGET", "_blank");

//literals
define("PMDMS_H1_TEXT", "Poor Man DMS Demo site");
define("PMDMS_LOGIN_PAGE_TEXT", "<p>This is a Poor Man DMS demo site. The password is 'demo'. Visit <a href='http://git.shotbingo.com/poor-man-dms/wiki/Home'>the Poor Man DMS wiki</a> for more information</p>");
define("PMDMS_LOGIN_BUTTON_TEXT", "Login");
define("PMDMS_LOGOUT_BUTTON_TEXT", "Logout");
define("PMDMS_ZIP_BUTTON_TEXT", "Download selected files");
define("PMDMS_PASSWORD_PLACE_HOLDER_TEXT", "Password");

//errors
define("PMDMS_ACCESS_ERROR", "Access error");
define("PMDMS_WRONG_PASSWORD", "Wrong password");
define("PMDMS_FILE_NOT_FOUND", "File not found");
define("PMDMS_NOTHING_TO_DOWNLOAD", "No file has been selected");
?>