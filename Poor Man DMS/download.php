<?php
/* Poor Man DMS v1.0
 * Copyright 2014 Alfonso da Silva (alfonsodasilva@gmail.com)
 * Licensed under MIT (http://git.shotbingo.com/poor-man-dms/raw/master/Poor%20Man%20DMS/license.txt)
 */
include 'config.php';

$rootdir = "";
if(isset($_SESSION['rootdir'])) {
	$rootdir = $_SESSION['rootdir'];
	if (! is_dir ( $rootdir )) {
		unset ( $_SESSION ['rootdir'] );
		header("Location: index.php");
		die();
	}
} else {
	header("Location: index.php?errorMsg=".urlencode(PMDMS_ACCESS_ERROR));
	die();
}

function checkFile($fileWithDir) {
	$items = explode(DIRECTORY_SEPARATOR, $fileWithDir);
	$file = end($items);
	return (preg_match (PMDMS_DOWNLOADABLE_FILE_REGEXP, $file));
}

if(isset($_GET["file"])) {
	$file = $_GET["file"];
	
	if(is_file($rootdir.$file)) {
		if (checkFile($file)) {
			header("Content-Type: ". PMDMS_DOWNLOADABLE_FILE_MIME_TYPE);
			header('Content-Length: ' . filesize($rootdir.$file));
			header("Content-disposition: attachment; filename=".PMDMS_DOWNLOADABLE_FILE_PREFIX.urlencode($file));
			readfile($rootdir.$file);
		} else {
			header("Location: index.php?errorMsg=".urlencode(PMDMS_FILE_NOT_FOUND));
			die();
		}
	} else {
		header("Location: index.php?errorMsg=".urlencode(PMDMS_FILE_NOT_FOUND));
		die();
	}
} else if(isset($_POST["file"])) {
	$fileArray = $_POST["file"];
	
	$count = count($fileArray);
	if($count == 0) {
		header("Location: index.php?errorMsg=".urlencode(PMDMS_NOTHING_TO_DOWNLOAD));
		die();
	}
	
	$file = tempnam(PMDMS_TMP_DIR, "zip");
	$zip = new ZipArchive();
	$zip->open($file, ZipArchive::OVERWRITE);
	for ($i = 0; $i < $count; $i++) {
		if (checkFile($fileArray[$i])) {
			$zip->addFile($rootdir.$fileArray[$i],PMDMS_DOWNLOADABLE_FILE_PREFIX.$fileArray[$i]);
		}
	}
	$zip->close();

	header("Content-Type: application/zip");
	header("Content-Length: " . filesize($file));
	header("Content-Disposition: attachment; filename=".PMDMS_DOWNLOADABLE_FILE_PREFIX."-".time().".zip");
	readfile($file);
	unlink($file);
} else {
	header("Location: index.php?errorMsg=".urlencode(PMDMS_FILE_NOT_FOUND));
	die();
}

?>